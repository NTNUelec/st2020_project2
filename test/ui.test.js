const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close(); 
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.focus('body > div > form > div:nth-child(2) > input[type=text]');
    await page.keyboard.type('Taiwan');

    await page.focus('body > div > form > div:nth-child(3) > input[type=text]');
    await page.keyboard.type('Taiwan');

    await page.click('body > div > form > div:nth-child(4) > button')

    await page.screenshot({path: 'test/screenshots/Welcome.png'});
    await browser.close();

})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.click('body > div > form > div:nth-child(4) > button')

    await page.screenshot({path: 'test/screenshots/Error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.focus('body > div > form > div:nth-child(2) > input[type=text]', {delay: 1000});
    await page.keyboard.type('Taiwan');

    await page.focus('body > div > form > div:nth-child(3) > input[type=text]', {delay: 1000});
    await page.keyboard.type('Taiwan');

    await page.click('body > div > form > div:nth-child(4) > button', {delay: 1000})

    await page.waitForSelector('messages > li > div.message__body > p');
    let message = await page.$eval('messages > li > div.message__body > p', (content) => content.innerHTML);
    expect(message).toBe('Hi Taiwan, Welcome to the chat app');
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 1000);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 1000);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(userName).toBe('R1');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 1000});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 1000});
    await page.keyboard.press('Enter', {delay: 1000}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });

    const page = await browser.newPage();    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 1000});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 1000});
    await page.keyboard.press('Enter', {delay: 1000}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    const page2 = await browser.newPage();    
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Rick', {delay: 1000});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 1000});
    await page2.keyboard.press('Enter', {delay: 1000}); 

    await page2.waitForSelector('#users > ol > li:nth-child(2)');    
    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    expect(member2).toBe('Rick');


    await browser.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.focus('body > div > form > div:nth-child(2) > input[type=text]', {delay: 1000});
    await page.keyboard.type('Happy');

    await page.focus('body > div > form > div:nth-child(3) > input[type=text]', {delay: 1000});
    await page.keyboard.type('Happy');

    await page.click('body > div > form > div:nth-child(4) > button', {delay: 1000})

    await page.waitForSelector('#message-form > button');
    let send = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(send).toBe('Send');
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.focus('body > div > form > div:nth-child(2) > input[type=text]', {delay: 100});
    await page.keyboard.type('Birthday');

    await page.focus('body > div > form > div:nth-child(3) > input[type=text]', {delay: 100});
    await page.keyboard.type('Birthday');

    await page.click('body > div > form > div:nth-child(4) > button', {delay: 100})

    await page.waitForSelector('#message-form > button');
    let send = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(send).toBe('Send');

    await page.focus('#message-form > input[type=text]', {delay: 100});
    await page.keyboard.type('Hi', {delay: 100}); 
    await page.keyboard.press('Enter', {delay: 1000});    
    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.focus('body > div > form > div:nth-child(2) > input[type=text]', {delay: 100});
    await page.keyboard.type('John');

    await page.focus('body > div > form > div:nth-child(3) > input[type=text]', {delay: 100});
    await page.keyboard.type('R1');

    await page.click('body > div > form > div:nth-child(4) > button', {delay: 100})

    await page.waitForSelector('#message-form > button');
    let send = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(send).toBe('Send');

    await page.focus('#message-form > input[type=text]', {delay: 100});
    await page.keyboard.type('Hi', {delay: 100});    
    await page.keyboard.press('Enter', {delay: 1000});

    const page2 = await browser.newPage();
    await page2.goto(url);

    await page2.focus('body > div > form > div:nth-child(2) > input[type=text]', {delay: 100});
    await page2.keyboard.type('Mike');

    await page2.focus('body > div > form > div:nth-child(3) > input[type=text]', {delay: 100});
    await page2.keyboard.type('R1');

    await page2.click('body > div > form > div:nth-child(4) > button', {delay: 100})

    await page2.waitForSelector('#message-form > button');
    let send2 = await page2.$eval('#message-form > button', (content) => content.innerHTML);
    expect(send2).toBe('Send');

    await page2.focus('#message-form > input[type=text]', {delay: 100});
    await page2.keyboard.type('Hello', {delay: 100}); 
    await page2.keyboard.press('Enter', {delay: 1000});

    await browser.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.focus('body > div > form > div:nth-child(2) > input[type=text]', {delay: 1000});
    await page.keyboard.type('Taiwan');

    await page.focus('body > div > form > div:nth-child(3) > input[type=text]', {delay: 1000});
    await page.keyboard.type('R1');

    await page.click('body > div > form > div:nth-child(4) > button', {delay: 1000})

    await page.waitForSelector('#send-location');
    let location = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(location).toBe('Send location');
    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.focus('body > div > form > div:nth-child(2) > input[type=text]', {delay: 1000});
    await page.keyboard.type('Taiwan');

    await page.focus('body > div > form > div:nth-child(3) > input[type=text]', {delay: 1000});
    await page.keyboard.type('Taiwan');

    await page.click('body > div > form > div:nth-child(4) > button', {delay: 1000})

    await page.waitForSelector('#send-location');
    let location = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(location).toBe('Send location');

    const [button] = await page.$x("//button[contains(., 'Send location')]");
    if (button) {
        await button.click();
    }
    //const send_location = await page.$('document.querySelector("#send-location")')
    //await send_location.click()
    //await page.click('#send-location', {delay: 1000})
    //await page.$eval('#send-location', button => button.click(), {delay: 1000});
    //await page.waitForNavigation();
    //await page.evaluate((sle) => document.querySelector('#send-location').click(), '#send-location'); 
    await browser.close();

})
